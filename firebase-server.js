const admin = require("firebase-admin");
const prerenderServer = require('./prerender-server');
const hash = require('object-hash');

exports = module.exports = function (option) {
    const app = admin.initializeApp({
        credential: admin.credential.cert(option),
        databaseURL: `https://${option.project_id}.firebaseio.com`
    }, option.project_id);
    

    app.database().ref(`request`).limitToLast(1).on('child_added', (snapshot) => {
        const url = snapshot.val();
        console.log('request render url', url);
        prerenderServer.render(`https://${option.project_id}.firebaseapp.com/${url}`).then(html => {
            app.database().ref(`request`).child(snapshot.key).remove();
            app.database().ref(`cached`).child(hash(url)).set(html);
        })
    })

    return app;
}