const fs = require('fs');
const path = require('path');
const firebaseServer = require('./firebase-server');

const credentialsPath = fs.readdirSync(path.join(__dirname,'./credentials'));
const admins = [];
credentialsPath.forEach(json => {
    if(/\.json/g.test(json)) {
        admins.push(firebaseServer(require(path.join(__dirname, './credentials/',json))))
        console.log('regis app ', json);
    }
})