const puppeteer = require('puppeteer');

const prerender = {};

prerender.browser = undefined;

prerender.start = async () => {
    if (prerender.browser) return prerender.browser;
    prerender.browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox'] });
    return prerender.browser;
}

prerender.render = async (url) => {
    const browser = await prerender.start();
    const page = await browser.newPage();
    await page.goto(url, { waitUntil: 'networkidle0' });
    const html = await page.evaluate(() => document.documentElement.outerHTML);
    await page.close();
    return html;
}

prerender.start();

exports = module.exports = prerender
